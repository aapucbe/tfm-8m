**Comunidad 0: Descartem**
- emmiartbook
- hsvale_idk0
- arigameplays
- KareenUrilo
- nickolalexa

Nodes corresponents a influencers de Mèxic.

**Comunidad 1: Descartem**
- lopezdoriga	
- lopezobrador_	
- DeniseDresserG	
- AlmaDeliaMC	
- AXL__tw	

Nodes relacionats amb Mèxic. El compte @lopezobrador_ correspon al president del país.

**Comunidad 2: Ens la quedem**
- IgualdadGob
- IreneMontero
- vitoquiles	
- MarDGamero
- CristinaSegui_	

Usuaris relacionats amb Espanya.

**Comunidad 3: Ens la quedem**
- RozalenMusic
- R3B3L24	
- macaballeroma
- LeonorWatling1
- PalomadelrioTVE	

Usuaris relacionats amb Espanya.

**Comunidad 4: Descartem**
- izkia
- cfelmerv	
- PiensaPrensa	
- nightmare_090	
- ColoColoFem	

Usuaris relacionats amb Chile. El compte @izkia és el compte de la ministra d'Interior.

**Comunidad 5: Descartem**
- desaparecidaorg	
- alferdez	
- Maia_Debowicz	
- infobae	
- vikidonda	

Nodes relacionats amb Argentina. @alferdez és  el president.

**Comunidad 6: Descartem**
- del_machismo	
- YessUrbina	
- PublimetroMX	
- sofffiaaa	
- anasofiacsn	

Usuaris relacionats amb Mèxic.

**Comunidad 7: Descartem**
- disalch	
- quinualover	
- MaricarmenAlvaP	
- rmapalacios	
- rebnat_zr	

Usuaris relacionats amb el Perú. L'usuaria @MaricarmenAlvaP es la presidenta del congrés.

**Comunidad 8: Descartem**
- DanannOficial	
- CTercermundista	
- PatoBullrich	
- Camila_Planas	
- AlvarodLamadrid	

Usuaris relacionats amb Llatinoamèrica.

**Comunidad 9: Descartem**
- Claudiadulcero	
- Bogota
- itamaria83	
- Col_Informa	
- causajustaco	

Usuaris relacionats amb Llatinoamèrica. @bogota correspon al compte de l'alcaldia de Bogotà.
 
**Comunidad 10: Ens la quedem**
- JordiCruzPerez	
- kamchatka_es	
- Hammer_moon	
- bibianacandia	
- sara05villa	

Usuaris relacionats amb Espanya.

**Comunidad 11: Descartem**
- ChalecosAmarill	
- LassoGuillermo	
- MeraZambrano_	
- RecNaturalesEC	
- LaKolmenaEC	

Usuaris relacionats amb l'Equador.

**Comunidad 12: Ens la quedem**
- PSOE	
- sanchezcastejon	
- gpscongreso	
- carmencalvo_	
- Nurgago	

Usuaris relacionats amb el govern d'Espanya i el PSOE.

**Comunidad 13: Descartem**
- CaraveoBertha	
- guruchuirer	
- Hans2412	
- ChumelTorres
- SinLinea_Mx

Usuaris relacionats amb Mèxic.

**Comunidad 14: Descartem**
- dannapaola	
- lalioficial	
- razeofficial	
- news_lolaindigo	
- Mundotendenciab	

Usuaris relacionats amb cantants argentines o grup de fans.

**Comunidad 15: Descartem**
- LuisLacallePou	
- ANAmassera50	
- OficialCAP	
- beatrizargimon	
- CosseCarolina	

Usuaris relacionats amb Uruguay. @LuisLacallePou es el president.

**Comunidad 16: Ens la quedem**
- ElHuffPost	
- 4septiembre2015	
- lechuga_feroz	
- danimartinezweb	
- antena3com	

Usuaris relacionats amb Espanya.

**Comunidad 17: Descartem**
- ONUMujeres	
- ONUMujeresMX	
- UNESCO_es	
- ameliarueda	
- ONU_es	

Comptes oficials com @ONU_es o @UNESCO_es. Podrien fer també referència a Espanya, però els descartarem perquè majoritàriament estan fent referència a altres comptes de Llatinoamèrica.

**Comunidad 18: Ens la quedem**
- FCBarcelona_es	
- realmadridfem	
- SpheraSports	
- AJFSFemenino	
- WorldPadelTour	

Comptes relacionats amb el futbol Espanyol femení.

**Comunidad 19: Descartem**
- berthamariaD	
- drewzecena	
- marcefitnessgt	
- DMujer33	
- JordanRodas

Usuaris relacionats amb Llatinoamèrica.

**Comunidad 20: Descartem**
- Martha_Hilda	
- alfredodelmazo	
- AlejandraDMV	
- SeguraNadya
- FrasesNorfiPC	

Usuaris relacionats amb Mèxic. @alfredodelmazo és el governador de l'Estat de Mèxic.

**Comunidad 21: Ens la quedem**
- begonavillacis	
- CiudadanosCs	
- AndaluciaJunta	
- aemprende	
- MADRID	

Usuaris relacionats amb Espanya.

**Comunidad 22: Descartem**
- Sharonnnna	
- pictoline	
- MafaldaDigital	
- amaraalzolay	
- sabribacal	

Nodes relacionats amb Argentina i Panamà.

**Comunidad 23: Ens la quedem**
- Filmin	
- consaludmental	
- ferminnegre	
- cemudis	
- VicepresidenRM

Usuaris relacionats amb Espanya.

**Comunidad 24: Ens la quedem**
- NuriaCSopena	
- EFEnoticias	
- efeminista_efe	
- LauraRdondo	
- _marian_is_back	

Usuaris relacionats amb Espanya.

**Comunidad 25: Ens la quedem**
- drusila_livia	
- Yosoycorra	
- HistoriaNG	
- Luisnostromo	
- rqlmartinez	

Comptes relacionats amb Espanya.

**Comunidad 26: Descartem**
- UNAM_MX	
- SRE_mx	
- inmujeres	
- aesquinca	
- CNDH	

Usuaris relacionats amb Mèxic.

**Comunidad 27: Descartem**
- CubaMINREX
- MercalOficial	
- MinMujerVe	
- BrendaGlezA1	
- MaikelMorenoVEN	

Usuaris relacionats amb Cuba i Veneçuela.

**Comunidad 28: Descartem**
- RiverPlate	
- TurDepAR	
- Independiente	
- SanLorenzo	
- Belgrano	

Usuaris relacionats amb el futbol argentí.

**Comunidad 29: Ens la quedem**
- esterexpositto
- bebi_fernandez	
- maaariasr_	
- ultimo_acorde	
- mariafdez992	

Usuaris relacionats amb Espanya. Gran importància de l'actriu Ester Expósito en aquesta comunitat.

**Comunidad 30: Ens la quedem**
- el_pais
- rtve
- elpais_america
- Newtral
- A3Noticias

Nodes relacionats amb mitjans de comunicació majoritàriament d'Espanya.

**Comunidad 31: Descartem**
- MarcoASolis
- saturninsta	
- anabreco	
- AuditorioMx	
- ANAGABRIELRL	

Usuaris relacionats amb Mèxic.

**Comunidad 32: Ens la quedem**
- Martasvm
- MG_GTCiencia	
- loretahur	
- _AEET_	
- CSICdivulga	

Usuaris relacionats amb Espanya.

**Comunidad 33: Ens la quedem**
- CCOO	
- fssccoo	
- UGT_Comunica	
- CCOOMadrid	
- UGT_Andalucia	

Usuaris relacionats amb sindiacats d'Espanya.

**Comunidad 34: Ens la quedem**
- cardanachama	
- FeijooGalicia	
- anaponton	
- Farodevigo	
- Xunta	

Usuaris relacionars amb Galicia.

**Comunidad 35: Descartem**
- AccionNacional	
- MarkoCortes	
- INEMexico	
- marianagc	
- AAtaydeR	

Usuaris relacionats amb Mèxic