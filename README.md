# Polarització del Dia Internacional de la Dona (8M) en Twitter i anàlisi del discurs d'odi.

### Resum del projecte

### Descripció dels arxius

__Jupyter notebooks/__
- *Anàlisi comunitats.ipynb*: En aquest notebook trobarem tots els càlculs relacionats amb l'estudi de l'estructura de les comunitats i els seus nodes. 
- *Anàlisi xarxa.ipynb*: En aquest altre tenim l'estudi de l'estructura de la xarxa general i les seues característiques.
- *Captura de Tweets.ipynb*: Ací hem fet la captura dels tweets mitjançant streaming.
- *Creació del graf.ipynb*: Amb els tweets obtinguts en l'anterior arxiu hem creat el graf utilitzant els processos que podem trobar en aquest ací.
- *Discurs odi.ipynb*: En aquest altre notebook hem capturat les interaccions amb els distints usuaris elegit i obtingut la freqüència d'aparició de termes relacionats amb el discurs de l'odi.
- *Temàtica principal.ipynb*: Finalment, tenim aquest arxiu on hem obtingut els tweets publicats a cada comunitat i l'anàlisi de la conversació que s'ha produït.

__Gephi/__
- *8m_k1_spain_coms.gephi*: En aquest projecte de Gephi podem veure el graf corresponent als nodes i arestes relacionats amb Espanya i algunes de les representacions visuals que hem fet servir a la memòria.

__tweets/__
- *tweets_comunitats/*: Conjunt de tweets produit en cada comunitat.
- *tweets_reaccions/*: Conjunt de tweets que reaccionen als usuaris elegits per analitzar.
- *tweets_streaming/*: En aquest directori trobarem un csv amb tot el conjunt de tweets capturats durant l'streaming sense cap tractament realitzat.

*comunitats_importants_espanya.md*: Top 10 nodes amb major grau d'entrada de cadascuna de les comunitats que representats el 90% de nodes i arestes relacionats elegits com a vinculats amb Espanya.

*discurs_odi.md*: Freqüència d'aparició de cadascun dels termes d'odi per a cada usuari elegit. Si no apareix un terme en un usuari concret significarà que la freqüència d'aparició del terme és 0.

*distribucio_tweets_reaccio.md*: Distribució del tipus de tweets que han reaccionat a l'usuari en l'anàlisi del discurs de l'odi. Distingim tweets propis publicats per l'usuari, citacions, respostes, mencions directes i mencions indirectes a l'usuari.

*selecció_comunitats_espanya.md*: Vista del top 5 nodes de les primeres comunitats de tota la xarxa abans de seleccionar les relacionades amb Espanya. Podem veure perquè hem descartat o elegit algunes com a relacionades amb Espanya.

*termes_odi.txt*: Llista dels termes d'odi. Està formada pels termes de __Torres__ (2017) més alguns que hem afegit, tal com està indicat a la memòria.

*TFM_Aaron_Puche_Benedito.pdf*: Memòria del treball.